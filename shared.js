//vamos a usar querySelector para obtener un elemento del DOM que coincida con las características
//que queremos, en querySelector podemos usar más de un atributo para la busqueda, como un elemento que
//contenga varias clases, querySelector siempre retornará un elemento
// var backdrop=document.querySelector('.someclass .anotherclass');
//si queremos retornar mas de un elemento, en forma de arreglo podemos usar querySelectorAll que hará eso
//querySelectorAll
//seleccionamos el elemento que contiene la clase backdrop y lo asignamos a una variable
let backdrop = document.querySelector('.backdrop');
//accedemos tambien al modal porque tambien necesitamos mostrar y ocultarlo
let modal = document.querySelector('.modal');
// agregamos de nuevo una referencia esta vez al boton de NO através de esta clase

let modalNoButton = document.querySelector('.modal__action--negative');


//podemos imprimir el elemento para ver lo que se ha seleccionado, esto nos retornará el html dele elemento
// console.log(backdrop);
//para ver la representacion dom con todos sus atributos usamos console.dir
//dentro podemos ver el atributo style, y dentro de ese atributo tenemos acceso
//a los estilos css inline de ese elemento, podemos ver que pueden no coincidir con los estilos que hemos agregado
//esto es porque son los estilos inline y normalmente nosotros agregamos estilos en archivos externos
//como son los estilos inline si los modificamos podemos ver los cambios que hagamos, ya que
//los estilos inline tienen alta prioridad
// console.dir(backdrop);
//modificamos el estilo display de none a block y esto se reflejará en nuestro html
// backdrop.style.display='block';

//creamos una referencia a todos nuestros botones que mostrarán el modal
//para agregar el evento click donde se mostrará
//usamos querySelectorAll para obtener un arreglo donde busquemos los botones
//que estan dentro de la clase plan, que seran los que activarán el modal
let selectPlanButtons = document.querySelectorAll('.plan button');
// console.dir(selectPlanButtons);
for (let i = 0; i < selectPlanButtons.length; i++) {
	//iteramos desde cero hasta la cantidad de botones que hayamos obtenido con nuestra consulta
	selectPlanButtons[i].addEventListener('click', () => {
		//y agregamos una función a cada botón donde
		//cambiemos el estilo de display none a display block
		// modal.style.display = 'block';
		// backdrop.style.display = 'block';
		// modal.className='open' //esto sobreescribe la completa lista de clases
		//usamos classList aquí, podemos agregar o quitar las clases de nuestro elemento usando classList
		//tambien tiene una función llamada toggle que hace la misma función pero para mayor entendimiento 
		//usaremos add para agregar las clases y remove para eliminarlas
		modal.classList.add('open');
		// para poder agregar la transición a la opacidad y cambiar el display de none a block y 
		// viceversa debemos de no hacer ambas al mismo tiempo, una forma en que podemos hacer esto
		//es primero cambiando el tipo de display, y luego con un pequeño delay, agregar la clase que activa 
		// la transición, que es la clase open, este delay lo hacemos agregando un timeOut, que hará que la clase open se
		//agregue 10 milisegundos mas tarde de que se asigno el display block, al hacer este cambio
		//el display no entrará en conflicto con el transform ya que no se dispararán al mismo tiempo
		// y ambos podrán funcionar en conjunto
		backdrop.style.display = "block";
		setTimeout(() => { backdrop.classList.add('open'); }, 10)
	});
}
//agregamos a nuestro backdrop y al boton No, el evento click
//y la referencia a la funcion, notar que solo es la referencia
//no se ejecuta ya que eso se hará al momento de dar click sobre cada uno de estos elementos
//la función está creada debajo de donde se agregan los escuchadores de eventos
//aquí estamos haciendo uso del hoisting

//modificamos el evento para agregar otra accion al backdrop, ahora
//no solo tiene que cerrar el modal sino tambien ocultar el sidebar
//ambas acciones se harán siempre, ya que no interfieren entre ellas
backdrop.addEventListener('click', () => {
	// mobileNav.style.display = 'none';
	//para ocultar el elemento eliminamos la clase open que se encarga de mostrarlo
	mobileNav.classList.remove('open');
	closeModal();
});

//validamos que exista el boton de no del modal antes de intentar agregar un evento en él
//esto es porque el botón de modal solo existe en la interfaz principal y este código se usa en todas 
//las interfaces
if (modalNoButton) {
	modalNoButton.addEventListener('click', closeModal);
}


function closeModal() {
	// modal.style.display = 'none';
	// backdrop.style.display = 'none';
	//para cerrar el modal de igual forma lo hacemos en vez de modificando los estilos
	//removiendo las clases que hacen que el elemento se muestre
	//agregamos una validación, ya que el código se usará en todas las interfaces y no todas
	//las interfaces tienen modal debemos validar que el modal exista, antes de intentar eliminar una clase 
	//de ese modal, solo se debe hacer con el modal ya que el backdrop se usará en todas las interfaces
	//para estar atrás del sidebar
	if (modal) {
		modal.classList.remove('open');
	}
	// para remover la clase open, el timeout debe ser más extenso y se debe primero
	//eliminar la clase open y luego cambiar el display a none, ademas de que el display
	//debe durar lo mismo que dura nuestra transición, para que se elimine el display despues de que la transición haya terminado
	//si pusieramos un tiempo menor al de la transición para el timeout que aplica el display none
	// este eliminaría del DOM al elemento antes de que terminara la transición lo que detendría abruptamente la transición
	backdrop.classList.remove('open');
	setTimeout(() => { backdrop.style.display = "none"; }, 200)

}

//agregamos las referencias al menu hamburguesa y a nuestro sidebar
let toggleButton = document.querySelector('.toggle-button');
let mobileNav = document.querySelector('.mobile-nav');

// vamos a agregar un selector a nuestro elemento cta, que tiene varias animaciones en él
// para poder agregar listeners de esas animaciones
let ctaButton = document.querySelector(".main-nav__item---cta")

//al menu hamburguesa le asignamos el mostrar cuando se hace click
//el sidebar, y el backdrop, y recordar que el backdrop al hacer click
//sobre el oculta el menu hamburguesa, entonces nuestro circuito está listo
toggleButton.addEventListener('click', () => {
	// backdrop.style.display='block';
	// mobileNav.style.display = 'block';
	mobileNav.classList.add('open');
	// aqui nuevamente agregamos el display block al elemento backdrop que inicialmente está oculto
	// y despues de que está agregado agregamos el delay y la clase open que agrega la animación al elemento
	backdrop.style.display = "block";
	setTimeout(() => { backdrop.classList.add('open'); }, 10)

});

// a los elementos del dom se les puede agregar listeners de eventos, que escuchan y disparan funciones cuando alguna parte
// del ciclo de vida de las animaciones es alcanzada, es decir, cuando la animación inicia, finaliza, o se completa una iteración

// para agregar el escuchador al elemento cta lo agregamos como hacemos para agregar cualquier otro listener
// este caso es para el escuchador de cuando la animación inicia, todos estos escuchadores de animación reciben un evento en su callback
// que al imprimirlo en consola nos dará más información de la animación en curso
ctaButton.addEventListener('animationstart', (e) => {
	console.log('animation started', e)
})
// para cuando la animación termina se usa este otro escuchador
ctaButton.addEventListener('animationend', (e) => {
	console.log('animation ended', e)
})
// y por último para cuando la animación alcanza una iteración, se usa este otro listener,
// este listener se ejecutara una vez con cada iteración, y dentro de sus detalles compartidos en el evento que recibe el callback
// podremos ver, entre otras cosas el tiempo transcurrido desde que inicio la animación para poder darnos una idea, 
// en la línea del tiempo de la animación, donde se ubica cada iteración de las que ejecutan el listener
ctaButton.addEventListener('animationiteration', (e) => {
	console.log('animation iteration', e)
})

// Para acceder a los nombrs de las propiedades de estilos de cada elemento del DOM
// podemos hacerlo de 2 formas, en JavaScript no se puede acceder a las propeidades
//usando guiones, entonces para poder acceder a una propiedad de CSS debemos usar camelCase
//es decir por ejemplo la propiedad background-image de css en el dom de JavaScript se usaría como
//backgroundImage, porque no se pueden usar guiones, una alternativa que tenemos sin embargo
//es acceder a las propiedades usando los corchetes [] y dentro usando strings
//de este modo podemos usar los guiones medio sin problemas
//Por ejemplo las 2 líneas de abajo hacen referencia a la propiedad backgroundImage
//del elemento del DOM, usando 2 sintaxis diferentes
console.dir(backdrop.style.backgroundImage);
console.dir(backdrop.style['background-image']);
